
from keras import callbacks
from keras import models
from keras.layers import advanced_activations
from keras.layers import core
from keras.layers import recurrent
from keras.layers import normalization

import numpy as np
from six.moves import cPickle
import sys

from quantum.dnn import base


class LSTM(base.DNN):
    def __init__(self):
        self.input_shape = None
        self.output_size = None
        self.dropout = []
        self.model = None
        # self.callbacks = []

    def build_model(self, input_shape, output_size,
                    hidden_layers=3,
                    hidden_neuro=[512, 512, 512],
                    dropout=0.2,
                    activation='softmax',
                    loss='mse',
                    optimizer='adam',
                    show_accuracy=False):
        self.input_shape = input_shape
        self.output_size = output_size
        assert hidden_layers >= 1

        if isinstance(hidden_neuro, int):
            neuros = [hidden_neuro] * hidden_layers
        else:
            neuros = hidden_neuro
        assert len(neuros) == hidden_layers

        model = models.Sequential()
        for i in range(0, hidden_layers):
            ret_seq = False if i == hidden_layers - 1 else True
            # last layers should set return_sequences=False
            if i == 0:
                model.add(recurrent.LSTM(neuros[i],
                                         input_shape=input_shape,
                                         return_sequences=ret_seq))
            else:
                model.add(recurrent.LSTM(neuros[i],
                                         return_sequences=ret_seq))
            model.add(normalization.BatchNormalization())
            model.add(advanced_activations.PReLU())
            if dropout > 0:
                model.add(core.Dropout(dropout))
        # model.add(core.TimeDistributedDense(output_size))
        model.add(core.Dense(output_size))
        model.add(core.Activation(activation))
        metrics = []
        if show_accuracy:
            metrics.append('accuracy')
        model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        self.model = model
        return self.model

    def save_model(self, file_path):
        if self.model is not None:
            # sys.setrecursionlimit(40000)
            # cPickle.dump(self.model, open(file_path, 'wb'))
            yaml_string = self.model.to_yaml()
            open(file_path, 'w').write(yaml_string)
            return True
        return False

    def load_model(self, file_path,
                   loss='mse',
                   optimizer='adam',
                   show_accuracy=False):
        # sys.setrecursionlimit(40000)
        # self.model = cPickle.load(open(file_path, 'rb'))
        self.model = models.model_from_yaml(open(file_path, 'r').read())
        metrics = []
        if show_accuracy:
            metrics.append('accuracy')
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        return self.model

    def save_weights(self, file_path):
        if self.model is not None:
            self.model.save_weights(file_path, overwrite=True)
            return True
        return False

    def load_weights(self, file_path):
        if self.model is not None:
            self.model.load_weights(file_path)
            return True
        return False

    def fit(self, x, y, epoch, batch_size=None, class_weight=None,
            earlystop=False, autosave_filepath=None, **kwargs):
        assert self.model is not None
        assert len(x) == len(y)
        if batch_size is None:
            batch_size = len(x)
        cbs = []
        if earlystop:
            earlystopping = callbacks.EarlyStopping(monitor='val_loss', patience=2)
            cbs.append(earlystopping)
        if autosave_filepath is not None:
            best_only=False
            if autosave_filepath == 'auto':
                autosave_filepath += '/tmp/weights-{epoch:02d}-{val_loss:.2f}.hdf5'
            elif not autosave_filepath.endswith('.hdf5'):
                autosave_filepath += '/weights.hdf5'
                best_only=True
            modelcheckpoint = callbacks.ModelCheckpoint(autosave_filepath, monitor='val_loss',
                verbose=1, save_best_only=best_only, mode='auto')
            cbs.append(modelcheckpoint)
        self.model.fit(x, y,
                       batch_size=batch_size,
                       nb_epoch=epoch,
                       class_weight=class_weight,
                       callbacks=cbs,
                       verbose=1,
                       **kwargs)

    def predict(self, x, batch_size=None, verbose=1, **kwargs):
        assert self.model is not None
        if batch_size is None:
            batch_size = len(x)
        preds = self.model.predict(x, batch_size=batch_size, verbose=verbose, **kwargs)
        return preds

    def predict_classes(self, x, batch_size=None, verbose=1, **kwargs):
        assert self.model is not None
        if batch_size is None:
            batch_size = len(x)
        preds = self.model.predict_classes(x, batch_size=batch_size, verbose=verbose, **kwargs)
        return preds

    def proba(self, x, batch_size=7, verbose=1, **kwargs):
        assert self.model is not None
        probas = self.model.predict_proba(x, verbose=verbose, **kwargs)
        return probas

    def validate(self, x, y, batch_size=None, show_accuracy=True, **kwargs):
        assert self.model is not None
        assert len(x) == len(y), 'Length of X and Y are not the same!'
        if batch_size is None:
            batch_size = len(x)
        score = self.model.evaluate(x, y, batch_size=batch_size, show_accuracy=show_accuracy, **kwargs)
        return score


class GRU(LSTM):
    def build_model(self, input_size, output_size,
                    hidden_layers=2,
                    hidden_neuro=512,
                    dropout=0.2,
                    activation='softmax',
                    loss='categorical_crossentropy',
                    optimizer='rmsprop',
                    class_mode='categorical'):
        assert hidden_layers > 1
        self.input_size = input_size
        self.output_size = output_size
        model = models.Sequential()
        for i in range(0, hidden_layers):
            input_neuro = hidden_neuro if i > 0 else input_size
            ret_seq = True if i < hidden_layers - 1 else False
            # last layers should set return_sequences=False
            model.add(recurrent.GRU(input_neuro, hidden_neuro,
                                    return_sequences=ret_seq))
            model.add(core.Dropout(dropout))
        model.add(core.Dense(hidden_neuro, output_size))
        model.add(core.Activation(activation))
        model.compile(loss=loss, optimizer=optimizer, class_mode=class_mode)
        self.model = model
        return self.model


class AELSTM(LSTM):
    def __init__(self):
        super(AELSTM, self).__init__()
        self.aes = None
        self.encoders = None

    def build_model(self, input_size, output_size,
                    hidden_layers=2,
                    hidden_neuro=[512, 512],
                    encoder_neuro=None,
                    dropout=0.2,
                    activation='softmax',
                    loss='categorical_crossentropy',
                    optimizer='rmsprop',
                    class_mode='categorical'):
        assert hidden_layers > 1
        self.input_size = input_size
        self.output_size = output_size
        model = models.Sequential()
        for i in range(hidden_layers-1):
            encoder=recurrent.LSTM(input_size, hidden_neuro[i],
                                   activation='sigmoid', return_sequences=True)
            decoder=recurrent.LSTM(hidden_neuro[i], input_size,
                                   activation='sigmoid', return_sequences=True)
            autoencoder = core.AutoEncoder(
                encoder=encoder,
                decoder=decoder,
                output_reconstruction=False, tie_weights=True
            )
            autoencoder.compile(loss='mean_squared_error', optimizer='adam')
        model.add(recurrent.LSTM(hidden_neuro, hidden_neuro,
                                 return_sequences=False))
        model.add(core.Dense(hidden_neuro, output_size))
        model.add(core.Activation(activation))
        model.compile(loss=loss, optimizer=optimizer, class_mode=class_mode)
        self.model = model
        return self.model