
from sklearn.preprocessing import StandardScaler, LabelEncoder

import numpy as np


class DNN(object):

    @staticmethod
    def scale_data(data, scaler=None):
        if not scaler:
            scaler = StandardScaler()
            scaler.fit(data)
        X = scaler.transform(data)
        return X, scaler

    @staticmethod
    def encode_labels(labels, encoder=None):
        if not encoder:
            encoder = LabelEncoder()
            encoder.fit(labels)
        y = encoder.transform(labels).astype(np.int32)
        return y, encoder

    @staticmethod
    def decode_labels(labels, encoder):
        y = encoder.inverse_transform(labels)
        return y