
import pandas as pd
import numpy as np
# import talib


def MA(df, n, type='simple', column='close'):
    assert isinstance(df, pd.DataFrame)
    assert type in ['simple', 'exp']
    ret = pd.DataFrame()
    ret[column] = df[column]
    if type == 'simple':
        ret['SMA_'+str(n)] = pd.rolling_mean(df[column], n, min_periods=0)
        ret['SMA_'+str(n)].fillna(0, inplace=True)
    elif type == 'exp':
        ret['EMA_'+str(n)] = pd.ewma(df[column], span=n, min_periods=0)
        ret['EMA_'+str(n)].fillna(0, inplace=True)
    del ret[column]
    return ret


def SMA(df, n, column='close'):
    return MA(df, n, type='simple', column=column)


def EMA(df, n, column='close'):
    return MA(df, n, type='exp', column=column)


def MACD(df, nfast=12, nslow=26, d=9, column='close'):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret[column] = df[column]
    macd_fast = pd.ewma(df[column], span=nfast)
    macd_slow = pd.ewma(df[column], span=nslow)
    ret['MACD_DIFF'] = macd_fast - macd_slow
    ret['MACD_DEA'] = pd.ewma(ret['MACD_DIFF'], span=d)
    ret['MACD'] = 2*(ret['MACD_DIFF'] - ret['MACD_DEA'])
    ret['MACD_X'] = 0
    macd_position = ret['MACD_DIFF'] > ret['MACD_DEA']
    ret.loc[macd_position[
               (macd_position == True) & (macd_position.shift() == False)].index,
           'MACD_X'] = 1
    ret.loc[macd_position[
               (macd_position == False) & (macd_position.shift() == True)].index,
           'MACD_X'] = -1
    del ret[column]
    return ret


# def tarsi(df, n=14):
#     return talib.RSI(df['close'].values, n)

def RSI(df, n=14, column='close'):
    """
    compute the n period relative strength indicator
    http://stockcharts.com/school/doku.php?id=chart_school:glossary_r#relativestrengthindex
    http://www.investopedia.com/terms/r/rsi.asp
    """
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret[column] = df[column]
    prices = df[column]
    deltas = np.diff(prices)
    seed = deltas[:n+1]
    up = seed[seed>=0].sum()/n
    down = -seed[seed<0].sum()/n
    rs = up/down
    rsi = np.zeros_like(prices)
    rsi[:n] = 100. - 100./(1.+rs)

    for i in range(n, len(prices)):
        delta = deltas[i-1] # cause the diff is 1 shorter

        if delta>0:
            upval = delta
            downval = 0.
        else:
            upval = 0.
            downval = -delta

        up = (up*(n-1) + upval)/n
        down = (down*(n-1) + downval)/n

        rs = up/down
        rsi[i] = 100. - 100./(1.+rs)

    ret['RSI_'+str(n)] = rsi
    del ret[column]
    return ret


def RSI2(df, n=6, column='close'):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret[column] = df[column]
    delta = df[column].diff()
    dUp, dDown = delta.copy(), delta.copy()
    dUp[dUp < 0] = 0
    dDown[dDown > 0] = 0

    wRolUp = pd.rolling_mean(dUp, n, min_periods=0)
    wRolDown = pd.rolling_mean(dDown.abs(), n, min_periods=0)

    wRS = wRolUp / wRolDown

    ret['WRSI_'+str(n)] = 100.0 - (100.0 / (1.0 + wRS))
    del ret[column]
    return ret


def KDJ(df, n=9, m=3, l=3):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['close'] = df['close']
    low_list = pd.rolling_min(df['low'], n, min_periods=0)
    low_list.fillna(value=pd.expanding_min(df['low']), inplace=True)
    high_list = pd.rolling_max(df['high'], n, min_periods=0)
    high_list.fillna(value=pd.expanding_max(df['high']), inplace=True)
    rsv = (df['close'] - low_list) / (high_list - low_list) * 100.0
    ret['KDJ_K'] = pd.ewma(rsv, com=m-1)
    ret['KDJ_K'].fillna(value=0, inplace=True)
    ret['KDJ_D'] = pd.ewma(ret['KDJ_K'], com=l-1)
    ret['KDJ_D'].fillna(value=0, inplace=True)
    ret['KDJ_J'] = 3 * ret['KDJ_K'] - 2 * ret['KDJ_D']
    # ret['KDJ_X'] = 0
    # kdj_position = ret['KDJ_K'] > ret['KDJ_D']
    # ret.loc[kdj_position[
    #            (kdj_position == True) & (kdj_position.shift() == False)].index,
    #        'KDJ_X'] = 1
    # ret.loc[kdj_position[
    #            (kdj_position == False) & (kdj_position.shift() == True)].index,
    #        'KDJ_X'] = -1
    ret['KDJ_X'] = ret['KDJ_K'] - ret['KDJ_D']
    del ret['close']
    return ret


def ROC(df, period=21):
    """
    ROC = [(Close - Close n periods ago) / (Close n periods ago)] * 100
    """
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['close'] = df['close']
    ret['ROC'] = (df['close'] - df['close'].shift(-period)) * 100.0 / df['close'].shift(-period)
    ret['ROC'].fillna(0, inplace=True)
    del ret['close']
    return ret


def BOLL(df, period=20, num_std_dev=2.0):
    """
    Bollinger BandWidth is an indicator that derives from Bollinger Bands, and
    measures the percentage difference between the upper band and the lower
    band.
    BandWidth decreases as Bollinger Bands narrow and increases as Bollinger
    Bands widen.
    Because Bollinger Bands are based on the standard deviation, falling
    BandWidth reflects decreasing volatility and rising BandWidth reflects
    increasing volatility.

    %B quantifies a security's price relative to the upper and lower Bollinger
    Band. There are six basic relationship levels:
    %B equals 1 when price is at the upper band
    %B equals 0 when price is at the lower band
    %B is above 1 when price is above the upper band
    %B is below 0 when price is below the lower band
    %B is above .50 when price is above the middle band (20-day SMA)
    %B is below .50 when price is below the middle band (20-day SMA)

    They were developed by John Bollinger.
    Bollinger suggests increasing the standard deviation multiplier to 2.1 for
    a 50-period SMA and decreasing the standard deviation multiplier to 1.9 for
    a 10-period SMA.
    """
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['close'] = df['close']
    sma_df = pd.rolling_mean(df['close'], period, min_periods=0)

    std_df = pd.rolling_std(df['close'], period, min_periods=0)

    ret['BOLL_UPPER'] = sma_df + std_df * num_std_dev
    ret['BOLL_MID'] = sma_df
    ret['BOLL_LOWER'] = sma_df - std_df * num_std_dev

    ret['BOLL_UPPER'].fillna(50, inplace=True)
    ret['BOLL_MID'].fillna(50, inplace=True)
    ret['BOLL_LOWER'].fillna(50, inplace=True)

    ret['BOLL_RANGE'] = ret['BOLL_UPPER'] - ret['BOLL_LOWER']
    ret['BOLL_BANDWIDTH'] = ret['BOLL_RANGE'] / ret['BOLL_MID']
    ret['BOLL_X'] = (ret['close'] - ret['BOLL_LOWER']) / ret['BOLL_RANGE']

    ret['BOLL_X'].replace([np.Inf, -np.Inf, np.NaN], 0, inplace=True)
    del ret['close']
    return ret


def BIAS(df, n=6, column='close'):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret[column] = df[column]
    sma = SMA(df, n, column=column)
    ret['BIAS'] = (df[column] - sma['SMA_%s' % n]) * 100.0 / sma['SMA_%s' % n]
    del ret[column]
    return ret.replace([np.Inf, -np.Inf, np.NaN], 0)


def DPO(df, n1=20, n2=10, n3=6, column='close'):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret[column] = df[column]
    sma = SMA(df, n1, column=column)
    ret['DPO'] = df['close'] - sma['SMA_%s' % n1].shift(n2)
    madpo = SMA(ret, n3, column='DPO')
    ret['MADPO'] = madpo['SMA_%s' % n3]
    ret['DPO_X'] = ret['DPO'] - ret['MADPO']
    del df[column]
    return ret.fillna(0)


def VR(df, n=24, column='volume'):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    # import ipdb;ipdb.set_trace()
    ret[column] = df[column]
    av = df[df['close'].diff() > 0]
    bv = df[df['close'].diff() < 0]
    ret['up'] = av[column]
    ret['up'].fillna(0, inplace=True)
    ret['down'] = bv[column]
    ret['down'].fillna(0, inplace=True)
    ret['up'] = pd.rolling_sum(ret['up'], n, min_periods=0)
    ret['down'] = pd.rolling_sum(ret['down'], n, min_periods=0)
    ret['VR'] = ret['up'] * 100.0 / ret['down']
    del ret['up']
    del ret['down']
    del ret[column]
    return ret.fillna(0)


def ASI(df, n1=26, n2=10):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['close'] = df['close']
    aa = df['high'] - df['close'].shift(1)
    ret['aa'] = aa.abs()
    bb = df['low'] - df['close'].shift(1)
    ret['bb'] = bb.abs()
    cc = df['high'] - df['low'].shift(1)
    ret['cc'] = cc.abs()
    dd = df['close'].shift(1) - df['open'].shift(1)
    ret['dd'] = dd.abs()
    r1 = ret['aa']+ret['bb']/2+ret['dd']/4
    r2 = ret['bb']+ret['aa']/2+ret['dd']/4
    r3 = ret['cc']+ret['dd']/4
    ret['r'] = r3
    ret.loc[
        ret[(ret['aa'] > ret['bb']) & (ret['aa'] > ret['cc'])].index,
        'r'
    ] = r1[(ret['aa'] > ret['bb']) & (ret['aa'] > ret['cc'])]
    ret.loc[
        ret[(ret['bb'] > ret['cc']) & (ret['bb'] > ret['aa'])].index,
        'r'
    ] = r2[(ret['bb'] > ret['cc']) & (ret['bb'] > ret['aa'])]
    xx = (df['close'].diff() +
          (df['close'] - df['open'])/2 +
          df['close'].shift() -
          df['open'].shift())
    si = 16*xx/ret['r']*ret[['aa', 'bb']].max(axis=1)
    ret['ASI'] = pd.rolling_sum(si, n1, min_periods=0)
    ret['ASIT'] = pd.rolling_mean(ret['ASI'], n2, min_periods=0)
    ret['ASI_X'] = ret['ASI'] - ret['ASIT']
    del ret['close']
    del ret['aa'], ret['bb'], ret['cc'], ret['dd'], ret['r']
    return ret.fillna(0)


def MTM(df, n1=12, n2=6, column='close'):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['MTM'] = df[column] - df[column].shift(n1)
    ret['MAMTM'] = pd.rolling_mean(ret['MTM'], n2, min_periods=0)
    return ret.replace([np.Inf, -np.Inf, np.NaN], 0)


def CHO(df, n1=10, n2=20, n3=6):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['mid'] = df['volume']*(2*df['close']-df['high']-df['low'])/(df['high']+df['low'])
    ret['CHO'] = pd.rolling_mean(ret['mid'], n1, min_periods=0) - pd.rolling_mean(ret['mid'], n2, min_periods=0)
    ret['MACHO'] = pd.rolling_mean(ret['CHO'], n3)
    del ret['mid']
    return ret.replace([np.Inf, -np.Inf, np.NaN], 0)


def WR(df, n=10):
    assert isinstance(df, pd.DataFrame)
    ret = pd.DataFrame()
    ret['close'] = df['close']
    ret['WR'] = (
        100.0 *
        (pd.rolling_max(df['high'], n, min_periods=0) - df['close']) /
        (pd.rolling_max(df['high'], n, min_periods=0) - pd.rolling_min(df['low'], n, min_periods=0))
    )
    del ret['close']
    return ret.replace([np.Inf, -np.Inf, np.NaN], 0)


if __name__ == '__main__':
    pass