
import tushare as ts
import pandas as pd
import datetime
import os
import six
import sys
from six.moves import cPickle

import logging

from quantum.common import datautil

log = logging.getLogger(__name__)


class Stock(object):

    def __init__(self, code, location=None, name=None):
        self.code = str(code)
        if not location:
            if self.code.startswith('6'):
                self.location = 'sh'
            else:
                self.location = 'sz'
        else:
            self.location = location
        self.stock_name = name

    @property
    def name(self):
        if self.stock_name:
            return self.stock_name
        try:
            ret_data = self.get_realtime_quotes()
            self.stock_name = ret_data.get('name')[0]
        except Exception as ex:
            log.debug('Fail to get stock name for {0}: {1}'.format(
                self.code, str(ex)))
        return self.stock_name

    @staticmethod
    def get_stocklist(detail=False, skip_startup=False):
        try:
            stocklist = ts.get_stock_basics()
            datautil.save_data(stocklist, 'stocklist.pkl')
        except Exception as ex:
            stocklist = datautil.load_data('stocklist.pkl')
            if not stocklist:
                raise ex
        if skip_startup:
            stocklist = stocklist[
                stocklist.index.to_series().str.startswith('3') == False]
        if detail:
            return stocklist
        else:
            stocklist = list(stocklist.name.keys())
            stocklist.sort()
            return stocklist

    @staticmethod
    def get_hs300s(detail=False):
        try:
            stocklist = ts.get_hs300s()
            datautil.save_data(stocklist, 'hs300s.pkl')
        except Exception as ex:
            stocklist = datautil.load_data('hs300s.pkl')
            if not stocklist:
                raise ex
        if detail:
            return stocklist
        else:
            stocklist = list(stocklist.code.values)
            stocklist.sort()
            return stocklist

    @staticmethod
    def tushare():
        return ts

    def get_realtime_quotes(self):
        try:
            result = ts.get_realtime_quotes(self.code)
        except Exception as ex:
            print('get_realtime_quotes error: %s' % str(ex))
            return
        if result is None or result.empty:
            return pd.DataFrame()
        return result

    def get_ticks(self, date):
        """
        :param date: datetime('2014-01-09') or '2014-01-09'
        """
        if isinstance(date, datetime.datetime):
            date_str = date.isoformat().split('T')[0]
        else:
            date_str = date
        result = ts.get_tick_data(self.code, date=date_str)
        if result is None or result.empty:
            return pd.DataFrame()
        if 'alert' in result.get('time')[0]:
            return pd.DataFrame()
        result['change'] = result['change'].replace('--', '0.0').astype(float)
        result = result[result['time'].str.len() == 8]
        result['datetime'] = pd.to_datetime(date_str+' '+result['time'], errors='raise')
        result = result.drop('time', axis=1)
        result = result[result['volume'] >= 0]
        result['price'] = result['price'].astype(float)
        return result.set_index('datetime')

    def get_history_ticks(self, begin, end):
        t = []
        while begin <= end:
            ticks = self.get_ticks(begin)
            if ticks is not None and ticks.any:
                t.append(ticks)
            begin += datetime.timedelta(days=1)
        if not t:
            return pd.DataFrame()
        ticks = pd.concat(t)
        return ticks

    def get_today_ticks(self):
        try:
            result = ts.get_today_ticks(self.code)
        except Exception as ex:
            print('get_today_ticks error: %s' % str(ex))
            return pd.DataFrame()
        if result is None or result.empty:
            return pd.DataFrame()
        return result

    def get_history_data(self, begin=None, end=None,
                         fq=None):
        """
        :param begin: datetime('2014-01-01') or '2014-01-01'
        :param end: datetime('2015-01-01') or '2015-01-01'
        :param fq: qfq, hfq, None
        :return:
        """
        param = {}
        param['autype'] = fq
        if isinstance(begin, datetime.datetime):
            param['start'] = begin.isoformat().split('T')[0]
        elif isinstance(begin, six.string_types):
            param['start'] = begin

        if isinstance(end, datetime.datetime):
            param['end'] = end.isoformat().split('T')[0]
        elif isinstance(end, six.string_types):
            param['end'] = end
        result = ts.get_h_data(self.code, **param)
        if result is None or result.empty:
            return pd.DataFrame()
        result['close'] = result['close'].astype(float)
        result['high'] = result['high'].astype(float)
        result['open'] = result['open'].astype(float)
        result['low'] = result['low'].astype(float)
        result['volume'] = result['volume'].astype(int)
        result['amount'] = result['amount'].astype(int)
        return result

    def get_datelist(self):
        result = self.get_history_data(
            begin=datetime.datetime(1989, 1, 1))

        if result is None or result.empty:
            return []
        return list(reversed(result.index))

    @staticmethod
    def transform_ticks(ticks, period='1min'):
        """
        :param period: '5min', '30min', '60min', '1min'
        :return:
        """
        assert isinstance(ticks, pd.DataFrame)
        if ticks is None or ticks.empty:
            return pd.DataFrame()
        datas = ticks[['price', 'volume', 'amount']]
        conversion = {'price': 'mean',
                      'volume': 'sum',
                      'amount': 'sum'}
        period_datas = datas.resample(period, how=conversion)
        period_datas = period_datas[period_datas['price'].notnull()]
        period_datas['open'] = datas['price'].resample(period, how='first')
        period_datas['close'] = datas['price'].resample(period, how='last')
        period_datas['high'] = datas['price'].resample(period, how='max')
        period_datas['low'] = datas['price'].resample(period, how='min')
        return period_datas

    @staticmethod
    def to_sql(data, table, engine, if_exists='fail'):
        assert isinstance(data, pd.DataFrame)
        data.to_sql(name=table, con=engine, if_exists=if_exists)

    @staticmethod
    def read_sql(table, engine, index_col=None):
        return pd.read_sql(sql=table, con=engine, index_col=index_col)


class Index(object):

    def __init__(self, code):
        self.code = str(code)
        assert self.code in self.get_indexlist(), '%s is not a valid index number' % self.code
        self.index_name = None
        self.index_map = {
            '000001': 'sh',
            '399001': 'sz',
            '000300': 'hs300',
            '000016': 'sz50',
            '399005': 'zxb',
            '399006': 'cyb',
        }

    @property
    def name(self):
        if self.index_name:
            return self.index_name
        try:
            ret_data = ts.get_index()
            ret_data = ret_data[ret_data['code'] == self.code]
            self.index_name = ret_data.get('name')[0]
        except Exception as ex:
            print('get %s name error: %s' % (self.code, str(ex)))
        return self.index_name

    @staticmethod
    def get_indexlist(detail=False):
        indexlist = ts.get_index()
        if detail:
            return indexlist
        else:
            dlist = list(indexlist['code'])
            dlist.sort()
            return dlist

    def get_realtime_quotes(self):
        result = ts.get_index()
        result = result[result['code'] == self.code]
        if result is not None and not result.empty:
            return result
        return pd.DataFrame()

    def get_history_data(self, begin=None, end=None):
        """
        :param start: datetime('2014-01-01') or '2014-01-01'
        :param end: datetime('2015-01-01') or '2015-01-01'
        :return:
        """
        param = {}
        param['index'] = True
        if isinstance(begin, datetime.datetime):
            param['start'] = begin.isoformat().split('T')[0]
        elif isinstance(begin, six.string_types):
            param['start'] = begin

        if isinstance(end, datetime.datetime):
            param['end'] = end.isoformat().split('T')[0]
        elif isinstance(end, six.string_types):
            param['end'] = end
        result = ts.get_h_data(self.code, **param)
        if result is None or result.empty:
            return pd.DataFrame()
        return result

    def get_ticks(self, date):
        assert self.code in self.index_map.keys(), 'Can not get ticks data for %s' % self.code
        code = self.index_map[self.code]
        start_str = date.isoformat().split('T')[0]
        date += datetime.timedelta(days=1)
        end_str = date.isoformat().split('T')[0]
        result = ts.get_hist_data(code, start=start_str, end=end_str, ktype='5')
        if result is None or result.empty:
            return pd.DataFrame()
        result = result['open', 'high', 'low', 'close', 'volume']
        return result.sort_index()

    def get_history_ticks(self, begin, end):
        t = []
        while begin <= end:
            ticks = self.get_ticks(begin)
            if ticks is not None and ticks.any:
                t.append(ticks)
            begin += datetime.timedelta(days=1)
        if not t:
            return pd.DataFrame()
        ticks = pd.concat(t)
        return ticks

    def transform_ticks(self, ticks, period='30min'):
        """
        :param period: '5min', '30min', '60min', '1min'
        :return:
        """
        assert isinstance(ticks, pd.DataFrame)
        if ticks is None or ticks.empty:
            return pd.DataFrame()
        datas = ticks[['price', 'volume']]
        conversion = {'price': 'mean',
                      'volume': 'sum'}
        period_datas = datas.resample(period, how=conversion)
        period_datas = period_datas[period_datas['price'].notnull()]
        period_datas['open'] = datas['price'].resample(period, how='first')
        period_datas['close'] = datas['price'].resample(period, how='last')
        period_datas['high'] = datas['price'].resample(period, how='max')
        period_datas['low'] = datas['price'].resample(period, how='min')
        return period_datas

if __name__ == '__main__':
    stock = Stock('002230')

