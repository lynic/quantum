
import datetime
import sqlalchemy
import pandas as pd
import argparse

from quantum import stock
from quantum.common import timeutil


class Ticks(object):
    def __init__(self, code, connection):
        # super(Ticks, self).__init__(code)
        self.code = str(code)
        self.stock = stock.Stock(self.code)
        # self.db_name = 'quant_ticks'
        self.engine = sqlalchemy.create_engine(connection)
            # 'mysql://elynn:525354@192.168.31.118/%(db_name)s?charset=utf8' %
            # {'db_name': self.db_name})
        self.default_start = datetime.datetime(2004, 1, 1)
        self.stock_name = self.stock.name

    def get_last_date(self):
        date_sql = ('select * from %(db_name)s.%(table)s order by datetime desc limit 1' %
                    {'table': self.code,
                     'db_name': self.db_name})
        try:
            his_df = pd.read_sql(sql=date_sql, con=self.engine, index_col='datetime')
        except Exception as ex:
            return None
        if his_df is None or his_df.empty:
            return None
        date = his_df.index.values[0]
        return timeutil.datetime64_to_datetime(date)

    def get_first_date(self):
        date_sql = ('select * from %(db_name)s.%(table)s order by datetime asc limit 1' %
                    {'table': self.code,
                     'db_name': self.db_name})
        try:
            his_df = pd.read_sql(sql=date_sql, con=self.engine, index_col='datetime')
        except Exception as ex:
            return None
        if his_df is None or his_df.empty:
            return None
        date = his_df.index.values[0]
        return timeutil.extract_date(timeutil.datetime64_to_datetime(date))

    def store_history_ticks(self, update=False):
        if update:
            start = self.get_last_date()
            if not start:
                start = self.default_start
            else:
                start += datetime.timedelta(days=1)
        else:
            start = self.default_start
        while start <= datetime.datetime.now():
            print('Fetching %(date)s ticks for %(code)s' % {'date': start, 'code': self.code})
            ticks = self.stock.get_ticks(date=start)
            if ticks is not None and not ticks.empty:
                print('Storing %(date)s ticks for %(code)s' % {'date': start, 'code': self.code})
                pd_ticks = self.stock.transform_ticks(ticks)
                pd_ticks.to_sql(name=str(self.code), con=self.engine, if_exists='append')
            start += datetime.timedelta(days=1)

    def load_history_ticks(self, start=None, end=None, resample=None):
        ticks_sql = 'select * from %(db_name)s.%(table)s'
        if start:
            ticks_sql += ' where datetime >= \'%(start)s\''
        if end:
            if start:
                ticks_sql += ' and datetime <= \'%(end)s\''
            else:
                ticks_sql += ' where datetime <= \'%(end)s\''
        ticks_sql += ' order by datetime desc'
        ticks_sql = ticks_sql % {'db_name': self.db_name,
                                 'table': self.code,
                                 'start': start,
                                 'end': end}
        try:
            ticks = pd.read_sql(sql=ticks_sql, con=self.engine, index_col='datetime')
        except Exception as ex:
            return None
        if ticks is None or ticks.empty:
            return None
        if resample:
            return self.transform_ticks(ticks, period=resample)
        else:
            return ticks

    @staticmethod
    def transform_ticks(ticks, period='5min'):
        assert isinstance(ticks, pd.DataFrame)
        if ticks is None or ticks.empty:
            return
        conversion = {'price': 'mean',
                      'open': 'first',
                      'close': 'last',
                      'high': 'max',
                      'low': 'min',
                      'volume': 'sum',
                      'amount': 'sum'
                      }
        period_datas = ticks.resample(period, how=conversion)
        period_datas = period_datas[period_datas['price'].notnull()]
        return period_datas


class IndexTicks(object):
    def __init__(self, code):
        self.code = str(code)
        self.stock = stock.Stock(self.code)
        self.db_name = 'quant_index_ticks'
        self.engine = sqlalchemy.create_engine(
            'mysql://elynn:525354@192.168.31.118/%(db_name)s?charset=utf8' %
            {'db_name': self.db_name})
        self.default_start = datetime.datetime(2004, 1, 1)
        self.stock_name = self.stock.name

    @staticmethod
    def transform_ticks(ticks, period='5min'):
        assert isinstance(ticks, pd.DataFrame)
        if ticks is None or ticks.empty:
            return
        conversion = {'open': 'first',
                      'close': 'last',
                      'high': 'max',
                      'low': 'min',
                      'volume': 'sum'
                      }
        period_datas = ticks.resample(period, how=conversion)
        period_datas = period_datas[period_datas['open'].notnull()]
        return period_datas


def handle_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', action="store_true", default=False,
                        dest='start')
    parser.add_argument('-c', '--continue', action="store_true", default=False,
                        dest='continue')
    parser.add_argument('-u', '--update', action="store_true", default=False,
                        dest='update')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = handle_args()
    if not args.start and not args.update:
        exit(1)
    count = 0
    stocklist = stock.Stock.get_stocklist(skip_startup=True)
    # stocklist = base.Stock.get_hs300s()
    # '000016' will encounter gbk unicodeerror
    stocklist.remove('000016')
    stocklist.remove('000709')
    total = len(stocklist)
    for code in stocklist:
        count += 1
        print('Progress %s/%s' % (count, total))
        tks = Ticks(code)
        print('Storing %s ticks data' % code)
        tks.store_history_ticks(update=args.update)