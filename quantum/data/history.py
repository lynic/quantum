
import datetime
import argparse
import pandas as pd
import sqlalchemy

from quantum import stock
from quantum.common import timeutil


class History(object):
    def __init__(self, code, connection):
        # super(Stock, self).__init__(code)
        self.code = str(code)
        self.stock = stock.Stock(self.code)
        # self.db_name = 'quant_history'
        self.engine = sqlalchemy.create_engine(connection)
        # 'mysql://elynn:525354@192.168.31.118/%(db_name)s?charset=utf8' %
        # {'db_name': self.db_name})
        self.default_start = datetime.datetime(1989, 1, 1)
        self.stock_name = self.stock.name

    def get_last_date(self, data_type='nofq'):
        assert data_type in ['nofq', 'hfq', 'qfq']
        table_name = '%s_%s' % (self.code, data_type)
        date_sql = ('select * from %(db_name)s.%(table)s order by date desc limit 1' %
                    {'table': table_name,
                     'db_name': self.db_name})
        try:
            his_df = pd.read_sql(sql=date_sql, con=self.engine, index_col='date')
        except Exception as ex:
            return None
        if his_df is None or his_df.empty:
            return None
        date = his_df.index.values[0]
        return timeutil.datetime64_to_datetime(date)

    def get_first_date(self, data_type='nofq'):
        assert data_type in ['nofq', 'hfq', 'qfq']
        table_name = '%s_%s' % (self.code, data_type)
        date_sql = ('select * from %(db_name)s.%(table)s order by date asc limit 1' %
                    {'table': table_name,
                     'db_name': self.db_name})
        try:
            his_df = pd.read_sql(sql=date_sql, con=self.engine, index_col='date')
        except Exception as ex:
            return None
        if his_df is None or his_df.empty:
            return None
        date = his_df.sort_index().index.values[0]
        return timeutil.datetime64_to_datetime(date)

    def store_history_data(self, update=False):
        if update:
            start = self.get_last_date(data_type='nofq')
            if not start:
                start = self.default_start
            else:
                start += datetime.timedelta(days=1)
            if_exists = 'append'
        else:
            start = self.default_start
            if_exists = 'replace'
        nofq_his = self.stock.get_history_data(begin=start)
        if nofq_his is not None and not nofq_his.empty:
            nofq_his.to_sql(name='%s_nofq' % self.code, con=self.engine, if_exists=if_exists)
        if update:
            start = self.get_last_date(data_type='hfq')
            if not start:
                start = self.default_start
            else:
                start += datetime.timedelta(days=1)
        hfq_his = self.stock.get_history_data(begin=start, fq='hfq')
        if hfq_his is not None and not hfq_his.empty:
            hfq_his.to_sql(name='%s_hfq' % self.code, con=self.engine, if_exists=if_exists)

    def load_history_data(self, data_type='nofq'):
        assert data_type in ['nofq', 'hfq', 'qfq']
        table_name = '%s_%s' % (self.code, data_type)
        try:
            his_data = pd.read_sql(sql=table_name, con=self.engine, index_col='date')
        except Exception as ex:
            return None
        if his_data is None or his_data.empty:
            return None
        else:
            return his_data.sort_index()

    def get_dates(self):
        table_name = '%s_%s' % (self.code, 'nofq')
        try:
            his_data = pd.read_sql(sql=table_name, con=self.engine, index_col='date')
        except Exception as ex:
            return None
        else:
            dates = his_data.index.values
            return dates


def handle_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', action="store_true", default=False,
                        dest='start')
    parser.add_argument('-c', '--continue', action="store_true", default=False,
                        dest='continue')
    parser.add_argument('-u', '--update', action="store_true", default=False,
                        dest='update')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = handle_args()
    if not args.start and not args.update:
        exit(1)
    count = 0
    stocklist = stock.Stock.get_stocklist()
    total = len(stocklist)
    for code in stocklist:
        count += 1
        print('Progress %s/%s' % (count, total))
        stk = History(code)
        print('Storing %s history data' % code)
        stk.store_history_data(update=args.update)