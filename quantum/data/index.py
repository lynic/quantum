
import datetime
import argparse
import pandas as pd
import sqlalchemy

from quantum import stock
from quantum.common import timeutil


class Index(object):
    def __init__(self, code, connection):
        self.code = code
        self.index = stock.Index(self.code)
        # self.db_name = 'quant_index'
        self.engine = sqlalchemy.create_engine(connection)
            # 'mysql://elynn:525354@192.168.31.118/%(db_name)s?charset=utf8' %
            # {'db_name': self.db_name})
        self.default_start = datetime.datetime(2004, 1, 1)

    def get_last_date(self):
        date_sql = ('select * from %(db_name)s.%(table)s order by date desc limit 1' %
                    {'table': self.code,
                     'db_name': self.db_name})
        try:
            his_df = pd.read_sql(sql=date_sql, con=self.engine, index_col='date')
        except Exception as ex:
            return None
        if his_df is None or his_df.empty:
            return None
        date = his_df.index.values[0]
        return timeutil.datetime64_to_datetime(date)

    def get_first_date(self):
        date_sql = ('select * from %(db_name)s.%(table)s order by date asc limit 1' %
                    {'table': self.code,
                     'db_name': self.db_name})
        try:
            his_df = pd.read_sql(sql=date_sql, con=self.engine, index_col='date')
        except Exception as ex:
            return None
        if his_df is None or his_df.empty:
            return None
        date = his_df.sort_index().index.values[0]
        return timeutil.datetime64_to_datetime(date)

    def store_index_data(self, update=False):
        if update:
            start = self.get_last_date()
            if not start:
                start = self.default_start
            else:
                start += datetime.timedelta(days=1)
            if_exists = 'append'
        else:
            start = self.default_start
            if_exists = 'replace'
        nofq_his = self.index.get_history_data(start=start)
        if nofq_his is not None and not nofq_his.empty:
            nofq_his.to_sql(name='%s' % self.code, con=self.engine, if_exists=if_exists)

    def load_index_data(self):
        try:
            his_data = pd.read_sql(sql=str(self.code), con=self.engine, index_col='date')
        except Exception as ex:
            return None
        else:
            return his_data.sort_index()


def handle_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', action="store_true", default=False,
                        dest='start')
    parser.add_argument('-c', '--continue', action="store_true", default=False,
                        dest='continue')
    parser.add_argument('-u', '--update', action="store_true", default=False,
                        dest='update')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = handle_args()
    if not args.start and not args.update:
        exit(1)
    count = 0
    indexlist = stock.Index.get_indexlist()
    total = len(indexlist)
    for code in indexlist:
        count += 1
        print('Progress %s/%s' % (count, total))
        stk = Index(code)
        print('Storing %s history data' % code)
        stk.store_index_data(update=args.update)