
import datetime
import dateutil
import pandas as pd


def str_to_datetime(string):
    return dateutil.parser.parse(string)


def extract_date(dt):
    date_str = dt.isoformat().split('T')[0]
    return dateutil.parser.parse(date_str)


def datetime_str_date(dt):
    date_str = dt.isoformat().split('T')[0]
    return date_str


def datetime_str_time(dt):
    time_str = dt.iiisoformat().split('T')[1]
    return time_str


def conbine_datetime(date, time):
    if isinstance(date, datetime.datetime):
        date_str = datetime_str_date(date)
    else:
        date_str = date
    return dateutil.parser.parse(date_str+time)


def datetime64_to_datetime(dt64):
    date = pd.to_datetime(dt64).to_datetime()
    return date