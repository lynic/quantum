
import os
import sys
from six.moves import cPickle


def save_data(data, file_name):
    data_dir = os.path.expanduser('~/.quantum/')
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    cPickle.dump(data, open(data_dir + file_name, 'wb'))
    return True


def load_data(file_name):
    data_dir = os.path.expanduser('~/.quantum/')
    result = None
    if os.path.exists(data_dir + file_name):
        result = cPickle.load(open(data_dir + file_name, 'r'))
    return result
