from setuptools import setup, find_packages
setup(
      name="quantum",
      version="0.1",
      description="quantum",
      author="Ethan Lynn",
      url="http://www.csdn.net",
      license="LGPL",
      packages= find_packages(),
      py_modules=['quantum'],
      )
